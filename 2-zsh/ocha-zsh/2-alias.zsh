alias -- -='cd -'

alias -g ...='cd ../..'
alias -g ....='cd ../../..'
alias -g .....='cd ../../../..'
alias -g ......='cd ../../../../..'
alias -g .......='cd ../../../../../..'

alias ls='ls -G'

alias l='ls -lah'
alias la='ls -lAh'
alias ll='ls -lh'
alias lt='tree | less'
alias lla='ls -lh -a'
alias lll='ls -lh | less'

alias grep='grep --color=tty'

alias gcw='gcc -Wall -Werror -Wextra'

alias vi='nvim'
alias vall='nvim -p *'
alias vic='nvim -p *.c'
function vfi(){
	nvim -p $(find . -name $1)
}

function grepa(){
	grep -A $1 $2 **/** 2> /dev/null
}

alias norminette="/usr/bin/ruby /usr/bin/norminette"
alias gchsort="git for-each-ref --sort=-committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'"

"____________________________________"
:syntax on
:set number relativenumber
:set cc=80
"____________________________________"
:set t_Co=256
:set t_ut=
:colorscheme codedark
"___________________________________"
:set backspace=indent,eol,start
:set list
:set showbreak=↪
:set listchars=tab:→\ ,trail:·,eol:¬,extends:…,precedes:…
hi NonText ctermfg=8
hi SpecialKey ctermfg=8
"___________________________________"
:set tabstop=4
:set softtabstop=4
:set shiftwidth=4
:set smartindent
:set autoindent
"____________________________________"
:set wildmenu
:set incsearch
:set hlsearch
"____________________________________"
:set cursorline
:set mouse=a
:set tabpagemax=100
"____________________________________"
:syntax sync minlines=100
:set synmaxcol=100
:set regexpengine=1
"____________________________________"
let g:syntastic_c_compiler_options = "-Wall -Wextra -Wfloat-equal -Wshadow -Wpointer-arith -Wcast-align -Wstrict-prototypes -Wwrite-strings -Waggregate-return -Wconversion -Wunreachable-code -Winit-self"
let g:syntastic_c_include_dirs = [ '../include', 'include', '../lib/include', 'lib/include', 'libmy/include', '../libmy/include.h', 'includes', 'libft/includes/', '../libft/includes/']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_enable_balloons = 1
let g:syntastic_enable_highlighting = 1
let g:syntastic_error_symbol = '⌘'
let g:syntastic_warning_symbol = '⦿'
"____________________________________"
let g:fastfold_savehook = 1
let g:tex_fold_enabled=1
let g:vimsyn_folding=1
let g:xml_syntax_folding = 1
let g:php_folding = 1
let g:perl_fold = 1
"____________________________________"
let g:user_emmet_install_global = 0
autocmd FileType html,css,php EmmetInstall
"____________________________________"
execute pathogen#infect()
syntax on
filetype plugin indent on
"____________________________________"
function! CommentToggle()
	execute ':silent! s/\([^ ]\)/\/\/ \1/'
	execute ':silent! s/^\( *\)\/\/ \/\/ /\1/'
endfunction

"smart indent when entering insert mode with i on empty lines"
function! IndentWithI()
	if len(getline('.')) == 0
		return "\"_cc"
	else
		return "i"
	endif
endfunction

"____________________________________"
let g:BASH_Ctrl_j = 'off'
:map <C-n> :Texplore<CR>
:nnoremap <space> :noh<CR>
:nnoremap <S-j> :tabn<CR>
:nnoremap <S-k> :tabp<CR>
:nnoremap <F3> :Stdheader<CR>
":nnoremap ; :"
:nnoremap <expr> i IndentWithI()
":nnoremap o o<esc>"
":nnoremap O O<esc>"
:nnoremap <C-k> :m.+1<CR>==
:nnoremap <C-j> :m.-2<CR>==
:vnoremap <C-k> :m'>+1<CR>gv=gv
:vnoremap <C-j> :m'<-2<CR>gv=gv
:xnoremap <space> :call CommentToggle()<CR>
"____________________________________"
" Go to the top of the file gg
" Go to the end of the file G
"
" Center screen "normal zz
" Go to top screan H
" Go to bottom of screen L
" Go to center of screen M
" Make down screen ctrl + e
" Make up screen ctrl + y
"
" Go to the begining of line 0
" Go to the end of line $
" Go to first char of line ^
" Go to the last char of line g_
"
" Go to the next choosen character f(x)
" Go to the previous choosen character F(x)
" Repeat previous f/F in the same way ;
" Repeat previous f/F in reversed way ,
"
" Set lowercase gu
" Set uppercase gU
"
" Use * to seek the word where you are
" w to go to the next word, e to go to the end of the next word
" b to go to the previous word
"
" Use :set filetype=php || html for indentation in php

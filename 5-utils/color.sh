for i in {0..255} ; do
	printf "\x1b[48;5;%sm%3d\e[0m " "$i" "$i"
	if (( i == 5 )) || (( i > 5 )) && (( (i-5) % 6 == 0 )); then
		printf "\n\n";
	fi
done
